/*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/

const express = require('express')
const bodyParser = require('body-parser')
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')
const axios = require('axios')

// declare a new express app
const app = express()
app.use(bodyParser.json())
app.use(awsServerlessExpressMiddleware.eventContext())

// Enable CORS for all methods
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
});


/** ********************
 * Example get method *
 **********************/

app.get('/events', (req, res) => {
  const events = ['Aloft Hotel', 'Coachella', 'Los Angeles Foodie Con', 'Orange County Fair']
  res.json({success: 'get call succeed!', url: req.url, events});
});

app.get('/photos', function(req, res) {
  axios.get('https://swapi.co/api/photos/')
    .then(response => {
      res.json({
        people: response.data.results,
        success: 'get call succeed!',
        url: req.url
      });
    })
    .catch(err => {
      res.json({
        error: 'error fetching data'
      });
    })
});

app.get('/events/*', (req, res) => {
  // Add your code here
  res.json({success: 'get call succeed!', url: req.url});
});

/** **************************
* Example post method *
****************************/

app.post('/events', (req, res) => {
  // Add your code here
  res.json({success: 'post call succeed!', url: req.url, body: req.body})
});

app.post('/events/*', (req, res) => {
  // Add your code here
  res.json({success: 'post call succeed!', url: req.url, body: req.body})
});

/****************************
* Example post method *
*************************** */

app.put('/events', (req, res) => {
  // Add your code here
  res.json({success: 'put call succeed!', url: req.url, body: req.body})
});

app.put('/events/*', (req, res) => {
  // Add your code here
  res.json({success: 'put call succeed!', url: req.url, body: req.body})
});

/****************************
* Example delete method *
*************************** */

app.delete('/events', (req, res) => {
  // Add your code here
  res.json({success: 'delete call succeed!', url: req.url});
});

app.delete('/events/*', (req, res) => {
  // Add your code here
  res.json({success: 'delete call succeed!', url: req.url});
});

app.listen(3000, () => {
    console.log("App started")
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app
