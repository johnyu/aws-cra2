import * as React from 'react';
import { API } from 'aws-amplify'
import { Storage } from 'aws-amplify'
import S3 from './S3';

type _props = {
    message: string
  }

type _state = {
    events: object
}

export default class Events extends React.Component<_props, _state> {
  state: _state = { events: [] }

  render() {
    return (<div>events</div>
    )
  }
}
