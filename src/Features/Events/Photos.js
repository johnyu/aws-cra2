import React, { Component } from 'react'
import { API } from 'aws-amplify'

export default class Photos extends Component {
  componentDidMount() {
    this.getPeople() // new
  }
  
  getPeople = async() => {
    try {
      const data = await API.get('amplifyrestapi', '/people')
      console.log('data from new people endpoint:', data)
    } catch (err) {
      console.log('error fetching data..', err)
    }
  }

  render() {
    return (
      <div>
        <span>Cool Photos</span>
      </div>
    )
  }
}
