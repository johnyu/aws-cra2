import * as React from 'react';
import styled from "styled-components";
import Wave from "../../Components/Wave";

const Wrapper = styled.footer`
  position: relative;
  padding-top: 10rem;
  padding-bottom: 2rem;
  background: ${props => props.theme.gradient.leftToRight};
  font-family: ${props => props.theme.fontFamily.heading};
  @media (max-width: ${props => props.theme.breakpoints.s}) {
    padding-top: 7rem;
  }
`;

const OptionalContent = styled.div`
  margin-top: 2rem;
  margin-bottom: 4rem;
  text-align: center;
  h1,
  h2 {
    color: ${props => props.theme.colors.white.light};
    text-align: center;
    margin: 0 auto;
    display: block;
  }
`;

const Content = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  color: ${props => props.theme.colors.black.base};
  a {
    color: ${props => props.theme.colors.black.base};
    &:hover {
      color: ${props => props.theme.colors.black.lighter};
    }
  }
  @media (max-width: ${props => props.theme.breakpoints.s}) {
    flex-direction: column;
  }
`;

const Item = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  text-shadow: ${props => props.theme.shadow.text.small};
  @media (max-width: ${props => props.theme.breakpoints.s}) {
    flex-direction: row;
    justify-content: center;
    margin-bottom: 1rem;
    a {
      margin-left: 0.5rem;
      margin-right: 0.5rem;
    }
  }
`;

const Important = styled(Item)`
  font-size: 1.2rem;
  a {
    color: ${props => props.theme.colors.white.base};
    &:hover {
      color: ${props => props.theme.colors.primary.base};
    }
  }
`;

const Copyright = styled.div`
  margin: 1rem 0;
  text-align: center;
  color: ${props => props.theme.colors.white.blue};
`;


const FooterWrapper = styled.div`
  display: grid;
  grid-template-columns: 0 1fr 0;
  @media (max-width: 600px) {
    display:grid;
    grid-template-columns: 0 1fr 0;
  }
`;
export const Footer = () => {
  return (
    <Wrapper>
      <Wave orientation="top" />
        <Content>
          <Important>
              <img
              width='342'
              height='54'
              src='http://www.sipandcolr.com/wp-content/uploads/2017/04/18072686_10156060187734167_1372693215_n.png'
              alt='' />
          </Important>
          <Item>
            <a href="https://www.facebook.com/sipandcolr/" target="_blank" rel="noopener noreferrer">
              Facebook
            </a>
            <a href="https://www.instagram.com/sipandcolr/" target="_blank" rel="noopener noreferrer">
              Instagram
            </a>
            <a href='https://www.linkedin.com/company-beta/18004676/' target='_blank'> Linkedin </a>
            <a href='https://twitter.com/sipandcolr?lang=en' target='_blank'> Twitter</a>
          </Item>
        </Content>
        <Copyright>Copyright © 2018. Sip And Colr LLC.</Copyright>
    </Wrapper>
  )
}
