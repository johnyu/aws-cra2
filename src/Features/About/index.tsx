import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';

type _props = {
  message: string
}
type _state = {
  count: number // like this
}
class About extends React.Component<any> {
  state: _state = { // second annotation for better type inference
    count: 0
  }
  render() {
    return (
      <div>{this.props.message} {this.state.count}</div>
    );
  }
}

export default About;