import * as React from 'react';
import styled from 'styled-components';

const AppWrapper: any = styled.div`
  display:grid;
  height: 100vh;
  grid-template-areas: "navbar navbar navbar"
                        "left_sidebar main_content rights_sidebar"
                        "footer footer footer";
  grid-template-columns: 1fr 4fr 1fr;
  grid-template-rows: 1fr 6fr 1fr;
  @media (max-width: 600px) {
    display:grid;
    height: 100vh;
		grid-template-areas: "navbar"
		                     "main_content"
		                     "footer";
		grid-template-columns: 100%;
		grid-template-rows: 1fr 4fr 1fr;
    background-color: red;
    background-color: 'blue';
  }
`;

export default AppWrapper;