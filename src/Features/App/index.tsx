import * as React from 'react';
import { Helmet } from 'react-helmet';
import styled, { ThemeProvider } from 'styled-components';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Navbar from './navbar';
import { Home } from '../Home';
import { Contact } from "../Contact";
import About from "../About";
import { NotFound } from "../NotFound";
import { Footer } from "../Footer";
import Events from '../Events';
import './App.css';
import theme from "../../theme";

const AppWrapper = styled.div`
  display:grid;
  height: 100vh;
  grid-template-areas: "navbar navbar navbar"
                        "main_content main_content main_content"
                        "footer footer footer";
  grid-template-columns: 1fr 4fr 1fr;
  grid-template-rows: 1fr 6fr 1fr;
  @media (max-width: 600px) {
    display:grid;
    height: 100vh;
		grid-template-areas: "navbar"
		                     "main_content"
		                     "footer";
		grid-template-columns: 100%;
		grid-template-rows: 1fr 4fr 1fr;
    background-color: 'blue';
  }
`;

const NavbarWrapper = styled.div`
  grid-area: navbar;
`;
const MainContentWrapper = styled.div`
    grid-area: main_content;
`;
const FooterWrapper = styled.div`
  grid-area: footer;
`;
const App = () => (
  <ThemeProvider theme={theme}>
    <AppWrapper>
      <NavbarWrapper>
        <Navbar/>
      </NavbarWrapper>
      <MainContentWrapper>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/events" component={Events} />
            <Route exact path="/contact" component={Contact} />
            <Route exact path="/about" component={About} />
            <Route exact path="*" component={NotFound} />
          </Switch>
        </BrowserRouter>
      </MainContentWrapper>
      <FooterWrapper>
        <Footer />
      </FooterWrapper>
    </AppWrapper>
  </ThemeProvider>
);

export default App;