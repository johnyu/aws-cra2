import * as React from 'react';
// @ts-ignore
import Headroom from 'react-headroom';
import styled from "styled-components";
interface Props {
  label?: React.ReactNode;
}
const HeadroomWrapper = styled.div`
  position: fixed;
  width: 100%;
  z-index: 2000;
`;

const Nav = styled.nav`
  display: flex;
  flexDirection: row;
  justify-content: space-between;
  padding: 10px;
`
const Links = styled.div`
  display: flex;
  flexDirection: row;
  justify-content: space-between;
  span {
    padding: 5px;
    color: white;
  }
`;
const Navbar = (props: Props) => {
  return (
    <HeadroomWrapper>
      <Headroom>
      <Nav>
        <div>
          <img height="30px" src='http://www.sipandcolr.com/wp-content/uploads/2017/04/18072686_10156060187734167_1372693215_n.png' alt='' />
        </div>
        <Links>
          <span>Home</span>
          <span>Blog</span>
          <span>Events</span>
          <span>Gallery</span>
          <span>About</span>
          <span>Contact</span>
        </Links>
        </Nav>
      </Headroom>
    </HeadroomWrapper>
)}
export default Navbar;