import * as React from 'react';
import facepaint from 'facepaint';
import styled, { keyframes, css } from 'styled-components';
import Landing from '../Landing';

const breakpoints = [768, 1024];
const mq = facepaint(breakpoints.map(bp => `@media (min-width: ${bp}px)`));

const breath = keyframes`
  from {
    transform: scale(1);
  }
  80% {
    transform: scale(1.2);
  }
  to {
    transform: scale(1);
  }
`;

const HeroWrapper = styled.div`
  height: 100%;
  align-items: center;
  display: grid;
  justify-content: center;
  background-image: url("http://www.sipandcolr.com/wp-content/uploads/2017/05/18596557_10156157469999167_407419752_o.png");
  background-size: cover;
  padding: 0 10%;
  text-align: center;
  ${mq({
    minHeight: [
      'calc(100vh - 300px)',
      'calc(100vh - 280px)',
      'calc(100vh - 220px)',
    ],
  })};
  h2 {
    line-height: 1;
    text-align: center;
    font-size: 2.2em;
    margin-bottom: 0.5em;
    letter-spacing: -0.03em;
    color: white;
    ${mq({
      fontSize: ['2.2em', '2.6em', '3em'],
    })};
  }
  img {
    max-width: 100%;
    margin: 20px 0;
  }
  a {
    align-items: center;
    background: #3366ff;
    border-radius: 3px;
    color: #fff;
    display: flex;
    font-size: 1em;
    height: 2.4em;
    width: 150px;
    line-height: 1;
    text-decoration: none;
    text-transform: uppercase;
    justify-content: center;
    padding: 0 2em;
    margin: 30px 0;
    animation: ${breath} 6s ease infinite;
  }
`;

export const Hero = () => {
  return (
    <>
    <HeroWrapper>
      <h2>Welcome to the Most Immersive Paint and Sip in OC</h2>
    </HeroWrapper>
    <Landing/>
    </>

  );
};