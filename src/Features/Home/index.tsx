import * as React from 'react';
import { Hero } from '../Hero'
export interface Props {
  label?: React.ReactNode;

}
export const Home = (props: Props) => {
  return (
    <div>
      <Hero />
    </div>
  );
};