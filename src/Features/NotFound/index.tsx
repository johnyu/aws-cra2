import * as React from 'react';

export interface Props {
  label?: React.ReactNode;
  children: React.ReactNode;

}
export const NotFound = (props: Props) => {
  return (
    <div>
      {props.label && <div>{props.label}</div>}
      {props.children}
    </div>
  );
};