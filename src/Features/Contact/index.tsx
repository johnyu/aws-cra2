import * as React from 'react';

export interface Props {
  label?: React.ReactNode;
  children: React.ReactNode;

}
export const Contact = (props: Props) => {
  return (
    <div>
      {props.label && <div>{props.label}</div>}
      {props.children}
    </div>
  );
};