import React, { Fragment } from 'react'
import styled from 'styled-components'

const ImageBarWrapper = styled.div`
  display:grid;
  grid-template-columns: minmax(0, 100%);
`
const PanelWrapper = styled.div`
  display:flex;
  flexDirection: row;
  max-width: 1268px;
  box-sizing: border-box;
  margin: 0px auto;
  width: 100%;
`
const ImgAd = styled.img`
  width: 100%;
  height: auto;
`
const ImgSmall = styled.img`
  background-size: 100%;
  background-size: cover;
  width: 60%;
`
const FeatureHeader = styled.div`
  display:grid;
  justify-content: center;
  align-items: center;
  margin-top: 100px;
`
const FeaturePage = styled.div`
  display: grid;
  grid-template-columns: repeat(2, minmax(150px, auto));
  grid-gap: 10px;
`
const EventsContainer = styled.div`
  display:grid;
  grid-template-columns: 1fr;
`
const EventsGrid = styled.div`
  display:grid;
  grid-template-columns: repeat(3, 300px);
  justify-content: center;
  align-items: center;
  margin-top: 80px;
  margin-bottom: 80px;
`
const EventsHeader = styled.div`
  display:grid;
  justify-content: center;
  align-items: center;
  margin-bottom: 80px;
`

const EventFigure = styled.figure`
  display:grid;
  justify-content: center;
  align-items: center;
`
const LandingWrapper = styled.div`
  max-width: 1268px;
  box-sizing: border-box;
  margin: 0px auto;
  width: 100%;
`;
/**
 * .wrapper {
  position: relative;
  padding-top: 56.25%; /* 16:9 Aspect Ratio */
// }
// img {
//   position: absolute;
//   left: 0;
//   top: 0;
//   width: 100%;
//   height: auto;
// }
//  * 
//  */
const Landing = () => <LandingWrapper>
                        <ImageBarWrapper>
                          <PanelWrapper>
                            <ImgAd src='http://www.sipandcolr.com/wp-content/uploads/2017/04/Group-3-1.png' alt='' />
                            <ImgAd src='http://www.sipandcolr.com/wp-content/uploads/2017/04/Group-21.png' alt='' />
                            <ImgAd src='http://www.sipandcolr.com/wp-content/uploads/2017/04/Group-4.png' alt='' />
                            <ImgAd src='http://www.sipandcolr.com/wp-content/uploads/2017/04/Group-5.png' alt='' />
                            <ImgAd src='http://www.sipandcolr.com/wp-content/uploads/2017/04/Group-6.png' alt='' />
                          </PanelWrapper>
                        </ImageBarWrapper>
                        <FeatureHeader>
                          <h2>Why Sip + Colr?</h2>
                          <h2>A few reasons why people love painting with us</h2>
                        </FeatureHeader>
                        <FeaturePage>
                          <div>
                            <ImgSmall src='http://www.sipandcolr.com/wp-content/uploads/2017/04/Community-Driven-1024x585.png' alt='' />
                          </div>
                          <div>
                            <ImgAd src='http://www.sipandcolr.com/wp-content/uploads/2017/04/Image_1.png' alt='' />
                          </div>
                        </FeaturePage>
                          <EventsContainer>
                            <EventsHeader>
                              <h2>Previous Events</h2>
                              <h2>Check out some of our past paintings</h2> 
                            </EventsHeader>
                            <EventsGrid>
                              <EventFigure>
                                <a href='http://www.sipandcolr.com/upcoming.html' target='_blank'><img src='http://www.sipandcolr.com/wp-content/uploads/elementor/thumbs/IMG_0540-n3izx8klbrbg16dk8e95a1kxb7jgbsz66se1przb8g.jpg' alt='IMG_0540' /></a>
                                <figcaption></figcaption>
                              </EventFigure>
                              <EventFigure>
                                <a href='http://www.sipandcolr.com/upcoming.html' target='_blank'><img src='http://www.sipandcolr.com/wp-content/uploads/elementor/thumbs/TsunAMI-h-n4vgvx754nczekqe8m8lagua7g8q8nsqmen8ew3b1c.jpg' alt='TsunAMI (h)' /></a>
                                <figcaption></figcaption>
                              </EventFigure>
                              <EventFigure>
                                <a href='http://www.sipandcolr.com/upcoming.html' target='_blank'><img src='http://www.sipandcolr.com/wp-content/uploads/elementor/thumbs/Silhouette-1-n3j0jpvms43bpfpzym2xevcsryqhf782a233x0ncf4.jpg' alt='Silhouette' /></a>
                                <figcaption></figcaption>
                              </EventFigure>
                              <EventFigure>
                                <a href='http://www.sipandcolr.com/upcoming.html' target='_blank'><img src='http://www.sipandcolr.com/wp-content/uploads/elementor/thumbs/Painting_4-2-1-n7f0sr1l4u4zoivn1yetlu88uvjtthaz7du8e4cxwg.png' alt='Painting_4 (2)' /></a>
                                <figcaption></figcaption>
                              </EventFigure>
                              <EventFigure>
                                <a href='http://www.sipandcolr.com/upcoming.html' target='_blank'><img src='http://www.sipandcolr.com/wp-content/uploads/elementor/thumbs/sunset-n4vgxhlgipi6r6gh3am7g2lxukir2i0mu5sd7hroo0.jpg' alt='sunset' /></a>
                                <figcaption></figcaption>
                              </EventFigure>
                              <EventFigure>
                                <a href='http://www.sipandcolr.com/upcoming.html' target='_blank'><img src='http://www.sipandcolr.com/wp-content/uploads/elementor/thumbs/Cali-Palm-Tree-n4vh164pacjm873yldwlpn5xlwdi7tmscdrtvib6ao.jpg' alt='Cali Palm Tree' /></a>
                                <figcaption></figcaption>
                              </EventFigure>
                              <EventFigure>
                                <a href='http://www.sipandcolr.com/upcoming.html' target='_blank'><img src='http://www.sipandcolr.com/wp-content/uploads/elementor/thumbs/Chrysha-a-taste-of-fall-H-n4vgw8h7ensf9wa0er444dztc2p4t11inyh267mkyo.jpg' alt='Chrysha - a taste of fall (H)'
                                  /></a>
                                <figcaption></figcaption>
                              </EventFigure>
                              <EventFigure>
                                <a href='http://www.sipandcolr.com/upcoming.html' target='_blank'><img src='http://www.sipandcolr.com/wp-content/uploads/elementor/thumbs/Tower-under-starry-night--n4vgxp461dshc25jvdv800pmlnhos2uhj7091pgja8.jpg' alt='Tower under starry night'
                                  /></a>
                                <figcaption></figcaption>
                              </EventFigure>
                              <EventFigure>
                                <a href='http://www.sipandcolr.com/upcoming.html' target='_blank'><img src='http://www.sipandcolr.com/wp-content/uploads/elementor/thumbs/Cherry-blossom-in-front-of-temple-h-n4vgwf22qi1fj60gcbyi3uc1hrspawrn0v1gj5ctr4.jpg' alt='Cherry blossom in front of temple (h)'
                                  /></a>
                                <figcaption></figcaption>
                              </EventFigure>
                              {/* <EventFigure>
                                <a href='http://www.sipandcolr.com/upcoming.html' target='_blank'><img src='http://www.sipandcolr.com/wp-content/uploads/elementor/thumbs/Love-in-the-air-n3j0gcmgahhc3qm4mno1z7yc59c4u8uotdxh0bn4ps.jpg' alt='Love in the air'
                                  /></a>
                                <figcaption></figcaption>
                              </EventFigure> */}
                            </EventsGrid>
                          </EventsContainer>
                      </LandingWrapper>

export default Landing;
